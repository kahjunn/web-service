import { SequelizeConnector } from '../tools/sequelizeConnector';
import { Sequelize } from 'sequelize';

const Admin = SequelizeConnector.define(
  'Admin',
  {
    id: {
      type: Sequelize.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true
    },
    firstName: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    lastName: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    email: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    password: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    phoneNo: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    createdAt: {
      type: Sequelize.DATE(),
      allowNull: true
    },
    updatedAt: {
      type: Sequelize.DATE(),
      allowNull: true,
    }
  }
);

export default Admin;