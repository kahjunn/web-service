import CPU from './CPU.model';
import GPU from './GPU.model';
import Motherboard from './Motherboard.model';
import RAM from './RAM.model';
import Storage from './Storage.model';
import Cooler from './Cooler.model';
import PSU from './PSU.model';
import Case from './Case.model';
import PCBuild from './PCBuild.model';
import BuildOrder from './BuildOrder.model';
import Customer from './Customer.model';
import Admin from './Admin.model';
import Promotion from './Promotion.model';

//Whichever model define first infront will have the foreign key
BuildOrder.belongsTo(Customer, { foreignKey: 'customerId', as: 'customer'});
BuildOrder.belongsTo(CPU, {foreignKey: 'cpuId', as: 'cpu'});
BuildOrder.belongsTo(GPU, { foreignKey: 'gpuId', as: 'gpu'});
BuildOrder.belongsTo(Motherboard, { foreignKey: 'moboId', as: 'motherboard'});
BuildOrder.belongsTo(RAM, { foreignKey: 'ramId', as: 'ram'});
BuildOrder.belongsTo(Storage, { foreignKey: 'storageId', as: 'storage'});
BuildOrder.belongsTo(PSU, { foreignKey: 'psuId', as: 'psu'});
BuildOrder.belongsTo(Cooler, { foreignKey: 'coolerId', as: 'cooler'});
BuildOrder.belongsTo(Case, { foreignKey: 'casingId', as: 'case'});

export {
  CPU,
  GPU,
  Motherboard,
  RAM,
  Storage,
  Cooler,
  PSU,
  Case,
  PCBuild,
  BuildOrder,
  Customer,
  Admin,
  Promotion
};








// Customer.hasMany(BuildOrder, { foreignKey: 'orderId', as: 'customer'});
// CPU.belongsTo(BuildOrder, {foreignKey: 'cpuId', as: 'cpu'});

// CPU.belongsTo(BuildOrder, { foreignKey: 'cpuId', as: 'cpu' });
// GPU.hasOne(BuildOrder, { foreignKey: 'gpuId', as: 'cpu' });
// Motherboard.hasOne(BuildOrder, { foreignKey: 'moboId', as: 'cpu' });
// RAM.hasOne(BuildOrder, { foreignKey: 'ramId', as: 'cpu' });
// Storage.hasOne(BuildOrder, { foreignKey: 'storageId', as: 'cpu' });
// PSU.hasOne(BuildOrder, { foreignKey: 'psuId', as: 'cpu' });
// Cooler.hasOne(BuildOrder, { foreignKey: 'coolerId', as: 'cpu' });
// Case.hasOne(BuildOrder, { foreignKey: 'casingId', as: 'cpu' });