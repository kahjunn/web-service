import { SequelizeConnector } from '../tools/sequelizeConnector';
import { Sequelize } from 'sequelize';

const PSU = SequelizeConnector.define(
  'PSU',
  {
    id: {
      type: Sequelize.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING(100),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    brand: {
      type: Sequelize.STRING(100),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    model: {
      type: Sequelize.STRING(80),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    wattage: {
      type: Sequelize.INTEGER(4),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    modularity: {
      type: Sequelize.STRING(20),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    rating: {
      type: Sequelize.STRING(20),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    color: { 
      type: Sequelize.STRING(20),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    sataConnector: {
      type: Sequelize.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    pcieConnector: {
      type: Sequelize.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    molexConnector: {
      type: Sequelize.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    price: {
      type: Sequelize.DECIMAL(10, 2),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    detail1: {
      type: Sequelize.TEXT,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    detail2: {
      type: Sequelize.TEXT,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    detail3: {
      type: Sequelize.TEXT,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    type: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    createdAt: {
      type: Sequelize.DATE(),
      allowNull: true
    },
    updatedAt: {
      type: Sequelize.DATE(),
      allowNull: true,
    }
  }
);

export default PSU;