import { SequelizeConnector } from '../tools/sequelizeConnector';
import { Sequelize } from 'sequelize';

const PCBuild = SequelizeConnector.define(
  'PCBuild',
  {
    id: {
      type: Sequelize.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING(80),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    cpu: {
      type: Sequelize.STRING(80),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    mobo: {
      type: Sequelize.STRING(80),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    memory: {
      type: Sequelize.STRING(80),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    storage: {
      type: Sequelize.STRING(80),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    gpu: {
      type: Sequelize.STRING(80),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    casing: {
      type: Sequelize.STRING(80),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    psu: {
      type: Sequelize.STRING(80),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    os: {
      type: Sequelize.STRING(80),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    cooler: {
      type: Sequelize.STRING(80),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    price: {
      type: Sequelize.DECIMAL(10, 2),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    description: {
      type: Sequelize.TEXT,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    createdAt: {
      type: Sequelize.DATE(),
      allowNull: true
    },
    updatedAt: {
      type: Sequelize.DATE(),
      allowNull: true,
    }
  }
);

export default PCBuild;