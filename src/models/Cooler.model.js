import { SequelizeConnector } from '../tools/sequelizeConnector';
import { Sequelize } from 'sequelize';

const Cooler = SequelizeConnector.define(
  'Cooler',
  {
    id: {
      type: Sequelize.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING(100),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    brand: {
      type: Sequelize.STRING(100),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    model: {
      type: Sequelize.STRING(80),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    minFanRPM: {
      type: Sequelize.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    maxFanRPM: {
      type: Sequelize.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    coolerType: {
      type: Sequelize.STRING(80),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    color: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    intelSocket: { 
      type: Sequelize.STRING,
      allowNull: true,
    },
    amdSocket: {
      type: Sequelize.STRING,
      allowNull: true,
    },
    price: {
      type: Sequelize.DECIMAL(10, 2),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    detail1: {
      type: Sequelize.TEXT,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    detail2: {
      type: Sequelize.TEXT,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    detail3: {
      type: Sequelize.TEXT,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    type: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    createdAt: {
      type: Sequelize.DATE(),
      allowNull: true
    },
    updatedAt: {
      type: Sequelize.DATE(),
      allowNull: true,
    }
  }
);

export default Cooler;