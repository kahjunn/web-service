import { SequelizeConnector } from '../tools/sequelizeConnector';
import { Sequelize } from 'sequelize';

const Promotion = SequelizeConnector.define(
  'Promotion',
  {
    id: {
        type: Sequelize.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true
      },
      name: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      createdAt: {
        type: Sequelize.DATE(),
        allowNull: true
      },
      updatedAt: {
        type: Sequelize.DATE(),
        allowNull: true,
      }
  }
);

export default Promotion;