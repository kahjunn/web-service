import { SequelizeConnector } from '../tools/sequelizeConnector';
import { Sequelize } from 'sequelize';

const GPU = SequelizeConnector.define(
  'GPU',
  {
    id: {
      type: Sequelize.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true
    },
    name: {
      type: Sequelize.STRING(80),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    brand: {
      type: Sequelize.STRING(100),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    model: {
      type: Sequelize.STRING(80),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    chipset: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    memory: {
      type: Sequelize.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    memoryType: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    architecture: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    coreClock: {
      type: Sequelize.INTEGER(5),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    boostClock: {
      type: Sequelize.INTEGER(5),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    gpuInterface: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    sliCrossfire: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    tdp: {
      type: Sequelize.INTEGER,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    color: {
      type: Sequelize.STRING(20),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    expansionSlotWidth: {
      type: Sequelize.INTEGER(3),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    displayPorts: {
      type: Sequelize.INTEGER(3),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    hdmiPorts: {
      type: Sequelize.INTEGER(3),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    dviPorts: {
      type: Sequelize.INTEGER(3),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    price: {
      type: Sequelize.DECIMAL(10, 2),
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    detail1: {
      type: Sequelize.TEXT,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    detail2: {
      type: Sequelize.TEXT,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    detail3: {
      type: Sequelize.TEXT,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    type: {
      type: Sequelize.STRING,
      allowNull: false,
      validate: {
        notEmpty: true
      }
    },
    createdAt: {
      type: Sequelize.DATE(),
      allowNull: true
    },
    updatedAt: {
      type: Sequelize.DATE(),
      allowNull: true,
    }
  }
); 

export default GPU;