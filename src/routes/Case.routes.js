import { Router } from 'express';
import { 
  getAllCase, 
  getOneCase, 
  createCase, 
  updateOneCase, 
  deleteOneCase 
} from '../controllers/Case.controller';

const router = new Router();

router.route('/').get(getAllCase);
router.route('/:id').get(getOneCase);
router.route('/create').post(createCase);
router.route('/update/:id').put(updateOneCase);
router.route('/delete/:id').delete(deleteOneCase);

export default router;