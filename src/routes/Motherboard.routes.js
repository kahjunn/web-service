import { Router } from 'express';
import { 
  getAllMobo, 
  getOneMobo, 
  createMobo, 
  updateOneMobo, 
  deleteOneMobo 
} from '../controllers/Motherboard.controller';

const router = new Router();

router.route('/').get(getAllMobo);
router.route('/:id').get(getOneMobo);
router.route('/create').post(createMobo);
router.route('/update/:id').put(updateOneMobo);
router.route('/delete/:id').delete(deleteOneMobo);

export default router;