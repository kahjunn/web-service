import { Router } from 'express';
import { 
  getAllGPU, 
  getOneGPU, 
  createGPU, 
  updateOneGPU, 
  deleteOneGPU 
} from '../controllers/GPU.controller';

const router = new Router();

router.route('/').get(getAllGPU);
router.route('/:id').get(getOneGPU);
router.route('/create').post(createGPU);
router.route('/update/:id').put(updateOneGPU);
router.route('/delete/:id').delete(deleteOneGPU);

export default router;