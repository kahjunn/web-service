import CPU from './CPU.routes';
import GPU from './GPU.routes';
import MOBO from './Motherboard.routes';
import RAM from './RAM.routes';
import Storage from './Storage.routes';
import PSU from './PSU.routes';
import Cooler from './Cooler.routes';
import Case from './Case.routes';
import PCBuild from './PCBuild.routes';
import BuildOrder from './BuildOrder.routes';
import Customer from './Customer.routes';
import Admin from './Admin.routes';
import Promotion from './Promotion.routes';

export default app => {
  app.use('/api/cpu/', CPU);
  app.use('/api/gpu/', GPU);
  app.use('/api/mobo/', MOBO);
  app.use('/api/ram/', RAM);
  app.use('/api/storage/', Storage);
  app.use('/api/psu/', PSU);
  app.use('/api/cooler/', Cooler);
  app.use('/api/case/', Case);
  app.use('/api/pcbuild/', PCBuild);
  app.use('/api/buildOrder/', BuildOrder);
  app.use('/api/customer/', Customer);
  app.use('/api/admin/', Admin);
  app.use('/api/promotion', Promotion);
}