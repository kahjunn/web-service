import { Router } from 'express';
import { 
  getAllRAM, 
  getOneRAM, 
  createRAM, 
  updateOneRAM, 
  deleteOneRAM 
} from '../controllers/RAM.controller';

const router = new Router();

router.route('/').get(getAllRAM);
router.route('/:id').get(getOneRAM);
router.route('/create').post(createRAM);
router.route('/update/:id').put(updateOneRAM);
router.route('/delete/:id').delete(deleteOneRAM);

export default router;