import { Router } from 'express';
import { 
  getAllPCBuild, 
  getOnePCBuild, 
  createPCBuild, 
  updateOnePCBuild, 
  deleteOnePCBuild 
} from '../controllers/PCBuild.controller';

const router = new Router();

router.route('/').get(getAllPCBuild);
router.route('/:id').get(getOnePCBuild);
router.route('/create').post(createPCBuild);
router.route('/update/:id').put(updateOnePCBuild);
router.route('/delete/:id').delete(deleteOnePCBuild);

export default router;