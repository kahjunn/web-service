import { Router } from 'express';
import { 
  getAllBuildOrder, 
  getOneBuildOrder, 
  createBuildOrder, 
  updateOneBuildOrder,
  updateOneBuildOrderStatus, 
  deleteOneBuildOrder ,
} from '../controllers/BuildOrder.controller';

const router = new Router();

router.route('/').get(getAllBuildOrder);
router.route('/:id').get(getOneBuildOrder);
router.route('/create').post(createBuildOrder);
// router.route('/update/:id').put(updateOneBuildOrder);
router.route('/updateStatus/:id').put(updateOneBuildOrderStatus);
router.route('/delete/:id').delete(deleteOneBuildOrder);

export default router;