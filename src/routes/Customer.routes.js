import { Router } from 'express';
import { 
  createCustomer,
  getOneCustomer, 
  updateOneCustomerDetails,
  getAllCustomerBO,
} from '../controllers/Customer.controller';

const router = new Router();

router.route('/create').post(createCustomer);
router.route('/:id').get(getOneCustomer);
router.route('/update/:id').put(updateOneCustomerDetails);
router.route('/orders/:id').get(getAllCustomerBO);

export default router;