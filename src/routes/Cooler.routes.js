import { Router } from 'express';
import { 
  getAllCooler, 
  getOneCooler, 
  createCooler, 
  updateOneCooler, 
  deleteOneCooler 
} from '../controllers/Cooler.controller';

const router = new Router();

router.route('/').get(getAllCooler);
router.route('/:id').get(getOneCooler);
router.route('/create').post(createCooler);
router.route('/update/:id').put(updateOneCooler);
router.route('/delete/:id').delete(deleteOneCooler);

export default router;