import { Router } from 'express';
import { 
  getAllPromotion,
  getOnePromotion,
  createPromotion,
  updateOnePromotionDetails,
  deleteOnePromotion
} from '../controllers/Promotion.controller';

const router = new Router();

router.route('/').get(getAllPromotion);
router.route('/:id').get(getOnePromotion);
router.route('/create').post(createPromotion);
router.route('/update/:id').put(updateOnePromotionDetails);
router.route('/delete/:id').delete(deleteOnePromotion);

export default router;