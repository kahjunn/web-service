import { Router } from 'express';
import { 
  createAdmin,
  getOneAdmin, 
  updateOneAdminDetails,
  getAllAdmin,
  deleteOneAdmin
} from '../controllers/Admin.controller';

const router = new Router();

router.route('/').get(getAllAdmin);
router.route('/:id').get(getOneAdmin);
router.route('/create').post(createAdmin);
router.route('/update/:id').put(updateOneAdminDetails);
router.route('/delete/:id').delete(deleteOneAdmin);

export default router;