import { Router } from 'express';
import { 
  getAllCPU, 
  getOneCPU, 
  createCPU, 
  updateOneCPU, 
  deleteOneCPU 
} from '../controllers/CPU.controller';

const router = new Router();

router.route('/').get(getAllCPU);
router.route('/:id').get(getOneCPU);
router.route('/create').post(createCPU);
router.route('/update/:id').put(updateOneCPU);
router.route('/delete/:id').delete(deleteOneCPU);

export default router;