import { Router } from 'express';
import { 
  getAllStorage, 
  getOneStorage, 
  createStorage, 
  updateOneStorage, 
  deleteOneStorage 
} from '../controllers/Storage.controller';

const router = new Router();

router.route('/').get(getAllStorage);
router.route('/:id').get(getOneStorage);
router.route('/create').post(createStorage);
router.route('/update/:id').put(updateOneStorage);
router.route('/delete/:id').delete(deleteOneStorage);

export default router;