import { Router } from 'express';
import { 
  getAllPSU, 
  getOnePSU, 
  createPSU, 
  updateOnePSU, 
  deleteOnePSU 
} from '../controllers/PSU.controller';

const router = new Router();

router.route('/').get(getAllPSU);
router.route('/:id').get(getOnePSU);
router.route('/create').post(createPSU);
router.route('/update/:id').put(updateOnePSU);
router.route('/delete/:id').delete(deleteOnePSU);

export default router;