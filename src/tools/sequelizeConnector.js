import dotenv from 'dotenv';
dotenv.config();
import { Sequelize } from 'sequelize';
import _ from 'lodash';

const nodeEnv = _.toUpper(process.env.NODE_ENV) || 'DEV';

const SequelizeConnector = new Sequelize(
  process.env[`${[nodeEnv]}_DB_NAME`] || '' ,
  process.env[`${[nodeEnv]}_DB_USER`],
  process.env[`${[nodeEnv]}_DB_PASSWORD`],
  {
    host: process.env.DB_HOST, 
    dialect: process.env.DB_DRIVER || 'mysql',
    port: process.env.DB_PORT || '3306',
    operatorAliases: false,
    logging: nodeEnv === 'DEV',
    pool: {
      max: 5,
      min: 0,
      accquire: 30000,
      idle: 10000
    },
    define: {
      freezeTableName: true,
      timestamps: true,
      paranoid: false,
    }
  }
);

export { Sequelize, SequelizeConnector };


