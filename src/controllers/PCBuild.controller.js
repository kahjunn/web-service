import { SequelizeConnector as Sequelize } from '../tools/sequelizeConnector';
import { PCBuild } from '../models';

export const createPCBuild = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body:', req.body);
    await PCBuild.create(req.body, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: 'New PCBuild created successfully'})
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

export const getAllPCBuild = async (req, res, next) => {
  try {
    const payload = await PCBuild.findAll({});
    return res.status(200).json({ message: 'PCBuild Data retrieved successfully', payload });
  } catch (error) {
    return next(error);
  }
};

export const getOnePCBuild = async (req, res, next) => {
  try {
    console.log('Request Body Params:', req.params.id);
    console.log('Request params:', req.params);
    const { id } = req.params;
    const payload = await PCBuild.findOne({
      where: { id }
    });
    return res.status(200).json({ message: `PCBuild Data ${id} retrieved successfully`, payload});
  } catch (error) {
    return next(error);
  }
};

export const updateOnePCBuild = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body', req.body);
    const { id } = req.params;
    const pcBuild = await PCBuild.findOne({ where: { id }});
    await pcBuild.update(req.body, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: `PCBuild Data ${id} updated successfully`});
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

export const deleteOnePCBuild = async (req, res, next) => {
  try {
    const { id } = req.params;
    const pcBuild = await PCBuild.findOne({ where: { id }});
    await pcBuild.destroy();
    return res.status(200).json({ message: `PCBuild Data ${id} deleted successfully`})
  } catch (error) {
    return next(error);
  }
};