import { SequelizeConnector as Sequelize } from '../tools/sequelizeConnector';
import { PSU } from '../models';

export const createPSU = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body:', req.body);
    const {
      name, 
      brand,
      model, 
      wattage, 
      modularity,
      rating,
      color,
      sataConnector,
      pcieConnector,
      molexConnector,
      price,
      detail1,
      detail2,
      detail3,
    } = req.body;

    const type="psu";
    await PSU.create({
      name: name, 
      brand: brand,
      model: model, 
      wattage: wattage, 
      modularity: modularity,
      rating: rating,
      color: color,
      sataConnector: sataConnector,
      pcieConnector: pcieConnector,
      molexConnector: molexConnector,
      type: type,
      price: price,
      detail1: detail1,
      detail2: detail2,
      detail3: detail3
    }, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: 'New PSU created successfully'})
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

export const getAllPSU = async (req, res, next) => {
  try {
    const payload = await PSU.findAll({});
    return res.status(200).json({ message: 'PSU Data retrieved successfully', payload });
  } catch (error) {
    return next(error);
  }
};

export const getOnePSU = async (req, res, next) => {
  try {
    console.log('Request Body Params:', req.params.id);
    console.log('Request params:', req.params);
    const { id } = req.params;
    const payload = await PSU.findOne({
      where: { id }
    });
    return res.status(200).json({ message: `PSU Data ${id} retrieved successfully`, payload});
  } catch (error) {
    return next(error);
  }
};

export const updateOnePSU = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body', req.body);
    const { id } = req.params;
    const cpu = await PSU.findOne({ where: { id }});
    await cpu.update(req.body, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: `PSU Data ${id} updated successfully`});
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

export const deleteOnePSU = async (req, res, next) => {
  try {
    const { id } = req.params;
    const cpu = await PSU.findOne({ where: { id }});
    await cpu.destroy();
    return res.status(200).json({ message: `PSU Data ${id} deleted successfully`})
  } catch (error) {
    return next(error);
  }
};