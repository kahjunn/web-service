import { SequelizeConnector as Sequelize } from '../tools/sequelizeConnector';
import { GPU } from '../models';

export const createGPU = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body:', req.body);
    const {
      name, 
      brand,
      model, 
      chipset, 
      memory, 
      memoryType,
      architecture, 
      coreClock,
      boostClock, 
      gpuInterface, 
      sliCrossfire, 
      tdp,
      color,
      expansionSlotWidth, 
      displayPorts,
      hdmiPorts,
      dviPorts,
      price,
      detail1,
      detail2,
      detail3,
    } = req.body;

    const type = "gpu";
    await GPU.create({
      name: name,
      brand: brand,
      model: model,
      chipset: chipset,
      memory: memory, 
      memoryType: memoryType, 
      architecture: architecture,
      coreClock: coreClock,
      boostClock: boostClock,
      gpuInterface: gpuInterface,
      sliCrossfire: sliCrossfire,
      tdp: tdp,
      color: color,
      expansionSlotWidth: expansionSlotWidth,
      displayPorts: displayPorts,
      hdmiPorts: hdmiPorts,
      dviPorts: dviPorts,
      type: type,
      price: price,
      detail1: detail1,
      detail2: detail2,
      detail3: detail3
    }, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: 'New GPU created successfully'})
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

export const getAllGPU = async (req, res, next) => {
  try {
    const payload = await GPU.findAll({});
    return res.status(200).json({ message: 'GPU Data retrieved successfully', payload });
  } catch (error) {
    return next(error);
  }
};

export const getOneGPU = async (req, res, next) => {
  try {
    console.log('Request Body Params:', req.params.id);
    console.log('Request params:', req.params);
    const { id } = req.params;
    const payload = await GPU.findOne({
      where: { id }
    });
    return res.status(200).json({ message: `GPU Data ${id} retrieved successfully`, payload});
  } catch (error) {
    return next(error);
  }
};

export const updateOneGPU = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body', req.body);
    const { id } = req.params;
    const cpu = await GPU.findOne({ where: { id }});
    await cpu.update(req.body, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: `GPU Data ${id} updated successfully`});
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

export const deleteOneGPU = async (req, res, next) => {
  try {
    const { id } = req.params;
    const cpu = await GPU.findOne({ where: { id }});
    await cpu.destroy();
    return res.status(200).json({ message: `GPU Data ${id} deleted successfully`})
  } catch (error) {
    return next(error);
  }
};