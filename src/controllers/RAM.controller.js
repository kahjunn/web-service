import { SequelizeConnector as Sequelize } from '../tools/sequelizeConnector';
import { RAM } from '../models';

export const createRAM = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body:', req.body);
    const {
      name,
      brand, 
      model,
      memoryType,
      memorySpeed, 
      modules, 
      color, 
      heatSpreader,  
      rgb, 
      price, 
      detail1, 
      detail2, 
      detail3 
    } = req.body;

    const type = "ram";
    await RAM.create({
      name: name,
      brand: brand, 
      model: model,
      memoryType: memoryType,
      memorySpeed: memorySpeed, 
      modules: modules, 
      color: color, 
      heatSpreader: heatSpreader,  
      rgb: rgb, 
      type: type,
      price: price,
      detail1: detail1,
      detail2: detail2,
      detail3: detail3,
    }, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: 'New RAM created successfully'})
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

export const getAllRAM = async (req, res, next) => {
  try {
    const payload = await RAM.findAll({});
    return res.status(200).json({ message: 'RAM Data retrieved successfully', payload });
  } catch (error) {
    return next(error);
  }
};

export const getOneRAM = async (req, res, next) => {
  try {
    console.log('Request Body Params:', req.params.id);
    console.log('Request params:', req.params);
    const { id } = req.params;
    const payload = await RAM.findOne({
      where: { id }
    });
    return res.status(200).json({ message: `RAM Data ${id} retrieved successfully`, payload});
  } catch (error) {
    return next(error);
  }
};

export const updateOneRAM = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body', req.body);
    const { id } = req.params;
    const cpu = await RAM.findOne({ where: { id }});
    await cpu.update(req.body, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: `RAM Data ${id} updated successfully`});
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

export const deleteOneRAM = async (req, res, next) => {
  try {
    const { id } = req.params;
    const cpu = await RAM.findOne({ where: { id }});
    await cpu.destroy();
    return res.status(200).json({ message: `RAM Data ${id} deleted successfully`})
  } catch (error) {
    return next(error);
  }
};