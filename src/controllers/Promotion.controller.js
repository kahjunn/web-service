import { SequelizeConnector as Sequelize } from '../tools/sequelizeConnector';
import { Promotion } from '../models';

export const createPromotion = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body:', req.body);
    await Promotion.create(req.body, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: 'New promotion created successfully'})
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

export const getAllPromotion = async (req, res, next) => {
  try {
    const payload = await Promotion.findAll({});
    return res.status(200).json({ message: 'Customer Data retrieved successfully', payload });
  } catch (error) {
    return next(error);
  }
};

export const getOnePromotion = async (req, res, next) => {
  try {
    console.log('Request Body Params:', req.params.id);
    console.log('Request params:', req.params);
    const { id } = req.params;
    const payload = await Promotion.findOne({
      where: { id }
    });
    return res.status(200).json({ message: `promotion Data ${id} retrieved successfully`, payload});
  } catch (error) {
    return next(error);
  }
};

export const updateOnePromotionDetails = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body', req.body);
    const { id } = req.params;
    const promotion = await Promotion.findOne({ where: { id }});
    await promotion.update(req.body, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: `promotion Data ${id} updated successfully`});
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};


export const deleteOnePromotion = async (req, res, next) => {
  try {
    const { id } = req.params;
    const promotion = await Promotion.findOne({ where: { id }});
    await promotion.destroy();
    return res.status(200).json({ message: `promotion Data ${id} deleted successfully`})
  } catch (error) {
    return next(error);
  }
};