import { SequelizeConnector as Sequelize } from '../tools/sequelizeConnector';
import { CPU } from '../models';
import { max } from 'lodash';

export const createCPU = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body:', req.body);
    const {
      name,
      brand,
      model,
      cache,
      coreCount,
      clockSpeed,
      boostClock,
      supportedSocket,
      threads,
      coreFamily,
      maxMemory,
      lithography,
      price,
      detail1,
      detail2,
      detail3
    } = req.body;

    const type = "cpu";
    await CPU.create({
      name: name,
      brand: brand,
      model: model,
      cache: cache,
      coreCount: coreCount,
      clockSpeed: clockSpeed,
      boostClock: boostClock,
      supportedSocket: supportedSocket,
      threads: threads,
      coreFamily: coreFamily,
      maxMemory: maxMemory,
      lithography: lithography,
      type: type,
      price: price,
      detail1: detail1,
      detail2: detail2,
      detail3: detail3,
    }, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: 'New CPU created successfully'})
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

export const getAllCPU = async (req, res, next) => {
  try {
    const payload = await CPU.findAll({});
    return res.status(200).json({ message: 'CPU Data retrieved successfully', payload });
  } catch (error) {
    return next(error);
  }
};

export const getOneCPU = async (req, res, next) => {
  try {
    console.log('Request Body Params:', req.params.id);
    console.log('Request params:', req.params);
    const { id } = req.params;
    const payload = await CPU.findOne({
      where: { id }
    });
    return res.status(200).json({ message: `CPU Data ${id} retrieved successfully`, payload});
  } catch (error) {
    return next(error);
  }
};

export const updateOneCPU = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body', req.body);
    const { id } = req.params;
    const cpu = await CPU.findOne({ where: { id }});
    await cpu.update(req.body, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: `CPU Data ${id} updated successfully`});
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

export const deleteOneCPU = async (req, res, next) => {
  try {
    const { id } = req.params;
    const cpu = await CPU.findOne({ where: { id }});
    await cpu.destroy();
    return res.status(200).json({ message: `CPU Data ${id} deleted successfully`})
  } catch (error) {
    return next(error);
  }
};