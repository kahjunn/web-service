import { SequelizeConnector as Sequelize } from '../tools/sequelizeConnector';
import { Customer, BuildOrder } from '../models';

export const createCustomer = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body:', req.body);
    // const { name } = req.body;
    // console.log('Customer Name: ', name);
    await Customer.create(req.body, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: 'New Customer created successfully'})
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

// export const getAllCustomer = async (req, res, next) => {
//   try {
//     const payload = await Customer.findAll({});
//     return res.status(200).json({ message: 'Customer Data retrieved successfully', payload });
//   } catch (error) {
//     return next(error);
//   }
// };

export const getOneCustomer = async (req, res, next) => {
  try {
    console.log('Request Body Params:', req.params.id);
    console.log('Request params:', req.params);
    const { id } = req.params;
    const payload = await Customer.findOne({
      where: { id }
    });
    return res.status(200).json({ message: `Customer Data ${id} retrieved successfully`, payload});
  } catch (error) {
    return next(error);
  }
};

export const updateOneCustomerDetails = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body', req.body);
    const { id } = req.params;
    const customer = await Customer.findOne({ where: { id }});
    await customer.update(req.body, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: `Customer Data ${id} updated successfully`});
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

export const getAllCustomerBO = async (req, res, next) => {
  try {
    console.log('Request Body Params:', req.params.id);
    console.log('Request params:', req.params);
    const { id } = req.params;
    const payload = await BuildOrder.findAll({
      where: { customerId: id }
    });
    return res.status(200).json({ message: `Build Order of Customer: ${id} retrieved successfully`, payload});
  } catch (error) {
    return next(error);
  }
};



// export const deleteOneCustomer = async (req, res, next) => {
//   try {
//     const { id } = req.params;
//     const customer = await Customer.findOne({ where: { id }});
//     await customer.destroy();
//     return res.status(200).json({ message: `Customer Data ${id} deleted successfully`})
//   } catch (error) {
//     return new(error);
//   }
// };