import { SequelizeConnector as Sequelize } from '../tools/sequelizeConnector';
import { Cooler } from '../models';

export const createCooler = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body:', req.body);
    const {
      name, 
      brand,
      model, 
      minFanRPM,
      maxFanRPM,
      coolerType,
      color,
      intelSocket,
      amdSocket,
      price,
      detail1,
      detail2,
      detail3,
    } = req.body;

    const type = "cooler";
    await Cooler.create({
      name: name,
      brand: brand,
      model: model,
      minFanRPM: minFanRPM,
      maxFanRPM: maxFanRPM,
      coolerType: coolerType,
      color: color,
      intelSocket: intelSocket,
      amdSocket: amdSocket,
      type: type,
      price: price,
      detail1: detail1,
      detail2: detail2,
      detail3: detail3
    }, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: 'New Cooler created successfully'})
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

export const getAllCooler = async (req, res, next) => {
  try {
    const payload = await Cooler.findAll({});
    return res.status(200).json({ message: 'Cooler Data retrieved successfully', payload });
  } catch (error) {
    return next(error);
  }
};

export const getOneCooler = async (req, res, next) => {
  try {
    console.log('Request Body Params:', req.params.id);
    console.log('Request params:', req.params);
    const { id } = req.params;
    const payload = await Cooler.findOne({
      where: { id }
    });
    return res.status(200).json({ message: `Cooler Data ${id} retrieved successfully`, payload});
  } catch (error) {
    return next(error);
  }
};

export const updateOneCooler = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body', req.body);
    const { id } = req.params;
    const cpu = await Cooler.findOne({ where: { id }});
    await cpu.update(req.body, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: `Cooler Data ${id} updated successfully`});
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

export const deleteOneCooler = async (req, res, next) => {
  try {
    const { id } = req.params;
    const cpu = await Cooler.findOne({ where: { id }});
    await cpu.destroy();
    return res.status(200).json({ message: `Cooler Data ${id} deleted successfully`})
  } catch (error) {
    return next(error);
  }
};