import { SequelizeConnector as Sequelize } from '../tools/sequelizeConnector';
import { Storage } from '../models';

export const createStorage = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body:', req.body);
    const {
      name, 
      brand,
      model, 
      storageType, 
      rpmSpeed, 
      volume,
      formFactor, 
      storageInterface,
      nvme,
      price,
      detail1,
      detail2,
      detail3,
    } = req.body;

    const type="storage"
    await Storage.create({
      name: name,
      brand: brand,
      model: model,
      storageType: storageType, 
      rpmSpeed: rpmSpeed, 
      volume: volume,
      formFactor: formFactor, 
      storageInterface: storageInterface,
      nvme: nvme,
      price: price,
      detail1: detail1,
      detail2: detail2,
      detail3: detail3,
      type: type
    }, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: 'New Storage created successfully'})
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

export const getAllStorage = async (req, res, next) => {
  try {
    const payload = await Storage.findAll({});
    return res.status(200).json({ message: 'Storage Data retrieved successfully', payload });
  } catch (error) {
    return next(error);
  }
};

export const getOneStorage = async (req, res, next) => {
  try {
    console.log('Request Body Params:', req.params.id);
    console.log('Request params:', req.params);
    const { id } = req.params;
    const payload = await Storage.findOne({
      where: { id }
    });
    return res.status(200).json({ message: `Storage Data ${id} retrieved successfully`, payload});
  } catch (error) {
    return next(error);
  }
};

export const updateOneStorage = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body', req.body);
    const { id } = req.params;
    const cpu = await Storage.findOne({ where: { id }});
    await cpu.update(req.body, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: `Storage Data ${id} updated successfully`});
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

export const deleteOneStorage = async (req, res, next) => {
  try {
    const { id } = req.params;
    const cpu = await Storage.findOne({ where: { id }});
    await cpu.destroy();
    return res.status(200).json({ message: `Storage Data ${id} deleted successfully`})
  } catch (error) {
    return next(error);
  }
};