import { SequelizeConnector as Sequelize } from '../tools/sequelizeConnector';
import { Admin } from '../models';

export const createAdmin = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body:', req.body);
    await Admin.create(req.body, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: 'New Admin User created successfully'})
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

export const getAllAdmin = async (req, res, next) => {
  try {
    const payload = await Admin.findAll({});
    return res.status(200).json({ message: 'Customer Data retrieved successfully', payload });
  } catch (error) {
    return next(error);
  }
};

export const getOneAdmin = async (req, res, next) => {
  try {
    console.log('Request Body Params:', req.params.id);
    console.log('Request params:', req.params);
    const { id } = req.params;
    const payload = await Admin.findOne({
      where: { id }
    });
    return res.status(200).json({ message: `Admin Data ${id} retrieved successfully`, payload});
  } catch (error) {
    return next(error);
  }
};

export const updateOneAdminDetails = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body', req.body);
    const { id } = req.params;
    const admin = await Admin.findOne({ where: { id }});
    await admin.update(req.body, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: `Admin Data ${id} updated successfully`});
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};


export const deleteOneAdmin = async (req, res, next) => {
  try {
    const { id } = req.params;
    const admin = await Admin.findOne({ where: { id }});
    await admin.destroy();
    return res.status(200).json({ message: `Admin Data ${id} deleted successfully`})
  } catch (error) {
    return next(error);
  }
};