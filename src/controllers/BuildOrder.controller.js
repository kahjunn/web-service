import { SequelizeConnector as Sequelize } from '../tools/sequelizeConnector';
import _ from 'lodash';
import { 
  BuildOrder,
  CPU,
  GPU,
  Motherboard,
  RAM,
  Storage,
  PSU,
  Cooler,
  Case,
  Customer
} from '../models';

export const createBuildOrder = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body:', req.body);
    const {
      cpuId,
      gpuId,
      moboId,
      ramId,
      storageId,
      psuId,
      coolerId,
      casingId
    } = req.body;

    //Dummy Data
    const customerId = 1;
    const status = "Pending Payment";
    const cpuPrice = await CPU.findOne({ raw: true, where: { id: cpuId }, attributes: ['price']});
    const gpuPrice = await GPU.findOne({ raw: true, where: { id: gpuId }, attributes: ['price']});
    const moboPrice = await Motherboard.findOne({ raw: true, where: { id: moboId }, attributes: ['price']});
    const ramPrice = await RAM.findOne({ raw: true, where: { id: ramId }, attributes: ['price']});
    const storagePrice = await Storage.findOne({ raw: true, where: { id: storageId }, attributes: ['price']});
    const psuPrice = await PSU.findOne({ raw: true, where: { id: psuId }, attributes: ['price']});
    const coolerPrice = await Cooler.findOne({ raw: true, where: { id: coolerId }, attributes: ['price']});
    const casePrice = await Case.findOne({ raw: true, where: { id: casingId }, attributes: ['price']});

    const priceCPU = cpuPrice.price;
    const priceGPU = gpuPrice.price;
    const priceMOBO = moboPrice.price;
    const priceRAM = ramPrice.price;
    const priceStorage = storagePrice.price;
    const pricePSU = psuPrice.price;
    const priceCooler = coolerPrice.price;
    const priceCase = casePrice.price;

    const pCPU = _.toString(priceCPU);
    const pGPU = _.toString(priceGPU);
    const pMOBO = _.toString(priceMOBO);
    const pRAM = _.toString(priceRAM);
    const pStorage = _.toString(priceStorage);
    const pPSU = _.toString(pricePSU);
    const pCooler = _.toString(priceCooler);
    const pCase = _.toString(priceCase);

    const p1 = parseFloat(pCPU);
    const p2 = parseFloat(pGPU);
    const p3 = parseFloat(pMOBO);
    const p4 = parseFloat(pRAM);
    const p5 = parseFloat(pStorage);
    const p6 = parseFloat(pPSU);
    const p7 = parseFloat(pCooler);
    const p8 = parseFloat(pCase);

    const total = (p1 + p2 + p3 + p4 + p5 + p6 + p7 + p8).toFixed(2);

    const orderNo = _.random(111111, 999999);
    console.log('Order Number', orderNo);

    console.log('$$$$$$$$$', _.isNaN(total));
    console.log('$$$$$$$$$', total);
    await BuildOrder.create({
      orderNo: orderNo,
      cpuId: cpuId,
      gpuId: gpuId,
      moboId: moboId,
      ramId: ramId,
      storageId: storageId,
      psuId: psuId,
      coolerId: coolerId,
      casingId: casingId,
      customerId: customerId,
      status: status,
      price: total
    }, { transaction }).then(result => {
      const boId = result.id;
      return res.json({ message: `Build Order Id created is ${boId}`, boId})
    });
    await transaction.commit();
    return res.status(200).json({ message: 'New Build Order created successfully'})
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

export const getAllBuildOrder = async (req, res, next) => {
  try {
    const payload = await BuildOrder.findAll({
      include: { 
        model: Customer, 
        as: 'customer'
      }
    });
    return res.status(200).json({ message: 'BuildOrder Data retrieved successfully', payload });
  } catch (error) {
    return next(error);
  }
};

export const getOneBuildOrder = async (req, res, next) => {
  try {
    console.log('Request Body Params:', req.params.id);
    console.log('Request params:', req.params);
    const { id } = req.params;
    const payload = await BuildOrder.findOne({
      where: { id },
      include: [
        {
          model: CPU,
          as: 'cpu'
        },
        {
          model: GPU,
          as: 'gpu'
        },
        {
          model: Motherboard,
          as: 'motherboard'
        },
        {
          model: RAM,
          as: 'ram'
        },
        {
          model: Storage,
          as: 'storage'
        },
        {
          model: PSU,
          as: 'psu'
        },
        {
          model: Cooler,
          as: 'cooler'
        },
        {
          model: Case,
          as: 'case'
        },
        {
          model: Customer,
          as: 'customer'
        }
      ]
    });
    return res.status(200).json({ message: `BuildOrder Data ${id} retrieved successfully`, payload});
  } catch (error) {
    return next(error);
  }
};

// export const updateOneBuildOrder = async (req, res, next) => {
//   const transaction = await Sequelize.transaction();
//   try {
//     console.log('Request Body', req.body);
//     const { id } = req.params;
//     const buildOrder = await BuildOrder.findOne({ where: { id }});
//     await buildOrder.update(req.body, { transaction });
//     await transaction.commit();
//     return res.status(200).json({ message: `BuildOrder Data ${id} updated successfully`});
//   } catch (error) {
//     await transaction.rollback();
//     return next(error);
//   }
// };

export const updateOneBuildOrderStatus = async (req, res, next) => {
  // const transacion = await Sequelize.transaction();
  try {
    console.log('Request Body', req.body);
    const { id } = req.params;
    const { status } = req.body;
    const buildOrder = await BuildOrder.findOne({ where: { id }});
    await buildOrder.update({ status }, { where: { id } });
    // await transaction.commit();
    return res.status(200).json({ message: `BuildOrder Status ${id} updated successfully`});
  } catch (error) {
    // await transaction.rollback();
    return next(error);
  }
};

export const deleteOneBuildOrder = async (req, res, next) => {
  try {
    const { id } = req.params;
    const buildOrder = await BuildOrder.findOne({ where: { id }});
    await buildOrder.destroy();
    return res.status(200).json({ message: `BuildOrder Data ${id} deleted successfully`})
  } catch (error) {
    return next(error);
  }
};
