import { SequelizeConnector as Sequelize } from '../tools/sequelizeConnector';
import { Motherboard } from '../models';

export const createMobo = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body:', req.body);
    const {
      name,
      brand, 
      model,
      formFactor,
      socket, 
      chipset, 
      maxMemory, 
      maxMemorySlot, 
      color, 
      sliCrossfire, 
      pcie16Slot,
      pcie8Slot,
      m2Slot,
      sataPort,
      onBoardEthernet,
      price, 
      detail1, 
      detail2, 
      detail3 
    } = req.body;

    const type="mobo"
    await Motherboard.create({
      name: name,
      brand: brand, 
      model: model,
      formFactor: formFactor,
      socket: socket, 
      chipset: chipset, 
      maxMemory: maxMemory, 
      maxMemorySlot: maxMemorySlot, 
      color: color, 
      sliCrossfire: sliCrossfire, 
      pcie16Slot: pcie16Slot,
      pcie8Slot: pcie8Slot,
      m2Slot: m2Slot,
      sataPort: sataPort,
      onBoardEthernet: onBoardEthernet,
      price: price, 
      detail1: detail1, 
      detail2: detail2, 
      detail3: detail3,
      type: type 
    }, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: 'New Motherboard created successfully'})
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

export const getAllMobo = async (req, res, next) => {
  try {
    const payload = await Motherboard.findAll({});
    return res.status(200).json({ message: 'Motherboard Data retrieved successfully', payload });
  } catch (error) {
    return next(error);
  }
};

export const getOneMobo = async (req, res, next) => {
  try {
    console.log('Request Body Params:', req.params.id);
    console.log('Request params:', req.params);
    const { id } = req.params;
    const payload = await Motherboard.findOne({
      where: { id }
    });
    return res.status(200).json({ message: `Motherboard Data ${id} retrieved successfully`, payload});
  } catch (error) {
    return next(error);
  }
};

export const updateOneMobo = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body', req.body);
    const { id } = req.params;
    const cpu = await Motherboard.findOne({ where: { id }});
    await cpu.update(req.body, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: `Motherboard Data ${id} updated successfully`});
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

export const deleteOneMobo = async (req, res, next) => {
  try {
    const { id } = req.params;
    const cpu = await Motherboard.findOne({ where: { id }});
    await cpu.destroy();
    return res.status(200).json({ message: `Motherboard Data ${id} deleted successfully`})
  } catch (error) {
    return next(error);
  }
};