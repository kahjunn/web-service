import { SequelizeConnector as Sequelize } from '../tools/sequelizeConnector';
import { Case } from '../models';

export const createCase = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body:', req.body);
    const {
      name, 
      brand,
      model, 
      formFactor,
      sidePanel,
      color,
      shroud,
      maxGPULength,
      bay2,
      bay3,
      supportedFormFactor,
      length,
      height,
      width,
      weight,
      price,
      detail1,
      detail2,
      detail3,
    } = req.body;

    const type = "casing";
    await Case.create({
      name: name, 
      brand: brand,
      model: model, 
      formFactor: formFactor,
      sidePanel: sidePanel,
      color: color,
      shroud: shroud,
      maxGPULength: maxGPULength,
      bay2: bay2,
      bay3: bay3,
      supportedFormFactor: supportedFormFactor,
      length: length,
      height: height,
      width: width,
      weight: weight,
      price: price,
      type: type,
      detail1: detail1,
      detail2: detail2,
      detail3: detail3,
    }, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: 'New Case created successfully'})
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

export const getAllCase = async (req, res, next) => {
  try {
    const payload = await Case.findAll({});
    return res.status(200).json({ message: 'Case Data retrieved successfully', payload });
  } catch (error) {
    return next(error);
  }
};

export const getOneCase = async (req, res, next) => {
  try {
    console.log('Request Body Params:', req.params.id);
    console.log('Request params:', req.params);
    const { id } = req.params;
    const payload = await Case.findOne({
      where: { id }
    });
    return res.status(200).json({ message: `Case Data ${id} retrieved successfully`, payload});
  } catch (error) {
    return next(error);
  }
};

export const updateOneCase = async (req, res, next) => {
  const transaction = await Sequelize.transaction();
  try {
    console.log('Request Body', req.body);
    const { id } = req.params;
    const cpu = await Case.findOne({ where: { id }});
    await cpu.update(req.body, { transaction });
    await transaction.commit();
    return res.status(200).json({ message: `Case Data ${id} updated successfully`});
  } catch (error) {
    await transaction.rollback();
    return next(error);
  }
};

export const deleteOneCase = async (req, res, next) => {
  try {
    const { id } = req.params;
    const cpu = await Case.findOne({ where: { id }});
    await cpu.destroy();
    return res.status(200).json({ message: `Case Data ${id} deleted successfully`})
  } catch (error) {
    return next(error);
  }
};