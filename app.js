import dotenv from 'dotenv';
dotenv.config();
import express from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import path from 'path';
import helmet from 'helmet';
import routes from './src/routes';
import PrettyError from 'pretty-error';
import _ from 'lodash';
import sequelize from 'sequelize';

//Pretty Error
const pe = new PrettyError();
pe.skipNodeFiles();
pe.skipPackage('express');

// Bring in Express.js
const app = express();

//HTTPS helmet
app.use(
  cors({
    credentials: true,
    origin: true
  })
);

//Request Parsing
app.use(bodyParser.json( {limit: '20mb'}));   
app.use(bodyParser.urlencoded({ limit: '20mb', extended: true }));

//HTTP Routing Middleware
app.all('/*', (req, res, next) => next());

//HTTP Routing Update 
app.put('/*', (req, res, next) => {
  req.body = _.omit(req.body, ['id']);
  if (_.isEmpty(req.body)) return next(new Error('Request body cannot be empty'));
  return next();
});

//Use Express to access routes
routes(app);

// Add Helmet to secure HTTP methods
app.use(helmet());

//Error Handling
app.use((err, req, res, next) => {
  if (err.name === 'ValidationError') {
    // eslint-disable-next-line no-param-reassign
    err.status = 400;
  }
  // eslint-disable-next-line no-console
  console.error(pe.render(err));
  
  res.status(err.status || 500);
  if (_.has(err, 'message')) {
    res.send(err.message);
  } else {
    let errorMessage = err;
    if (_.isArray(errorMessage)) {
      errorMessage = _.join(errorMessage, '\n');
    } else if (!_.isString(errorMessage)) {
      errorMessage = JSON.stringify(errorMessage);
    }
    
    res.send(errorMessage);
    // res.send(_.isString(err) ? err : JSON.stringify(err));
  }
  return next();
});

//To listen if the PORT is listening
const nodeEnv = _.toUpper(process.env.NODE_ENV) || 'DEV';

app.listen(process.env[`${[nodeEnv]}_PORT`], () => {
  const appName = process.env[`${[nodeEnv]}_APP_NAME`] || '';
  console.log(`${appName} APIs is running on PORT ${process.env[`${[nodeEnv]}_PORT`]}`); // eslint-disable-line no-console
});

export default app;
  
  
  




//Port to listen on
// const PORT = 3000; 

// const path = require("path");




//Test DB connection
// try {
//   await sequelize.authenticate();
//   console.log('Connection has been established successfully.');
// } catch (error) {
//   console.error('Unable to connect to the database:', error);
// }



  
  //Create Express Router
  // const router = express.Router();
  // app.use(router);
  
  //DB Connection 
  // require("./src/database/connection");

  // const nodeEnv = _.toUpper(process.env.NODE_ENV) || 'DEV';
// const PORT = process.env[`${[nodeEnv]}_PORT`];

// app.listen(PORT, err => {
  //   if (err) return console.log(`Cannot Listen on PORT: ${PORT}`);
  //   console.log(`Server is running on http://localhost:${PORT}/`);
  // });
  