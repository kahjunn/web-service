module.exports = {
  up: (queryInterface, Sequelize) => 
    queryInterface.createTable('BuildOrder', {
      id: {
        type: Sequelize.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true
      },
      orderNo: {
        type: Sequelize.INTEGER(6),
        allowNull: false,
        validate: { notEmpty: true }, 
      },
      cpuId: {
        type: Sequelize.INTEGER.UNSIGNED,
        allowNull: false,
        validate: { notEmpty: true },
        references: {
          model: 'CPU',
          key: 'id'
        }
      },
      moboId: {
        type: Sequelize.INTEGER.UNSIGNED,
        allowNull: false,
        validate: { notEmpty: true },
        references: {
          model: 'Motherboard',
          key: 'id'
        }
      },
      ramId: {
        type: Sequelize.INTEGER.UNSIGNED,
        allowNull: false,
        validate: { notEmpty: true },
        references: {
          model: 'RAM',
          key: 'id'
        }
      },
      storageId: {
        type: Sequelize.INTEGER.UNSIGNED,
        allowNull: false,
        validate: { notEmpty: true },
        references: {
          model: 'Storage',
          key: 'id'
        }
      },
      gpuId: {
        type: Sequelize.INTEGER.UNSIGNED,
        allowNull: false,
        validate: { notEmpty: true },
        references: {
          model: 'GPU',
          key: 'id'
        }
      },
      casingId: {
        type: Sequelize.INTEGER.UNSIGNED,
        allowNull: false,
        validate: { notEmpty: true },
        references: {
          model: 'Case',
          key: 'id'
        }
      },
      psuId: {
        type: Sequelize.INTEGER.UNSIGNED,
        allowNull: false,
        validate: { notEmpty: true },
        references: {
          model: 'PSU',
          key: 'id'
        }
      },
      coolerId: {
        type: Sequelize.INTEGER.UNSIGNED,
        allowNull: false,
        validate: { notEmpty: true },
        references: {
          model: 'Cooler',
          key: 'id'
        }
      },
      customerId: {
        type: Sequelize.INTEGER.UNSIGNED,
        allowNull: false,
        validate: { notEmpty: true },
        references: {
          model: 'Customer',
          key: 'id'
        }
      },
      status: {
        type: Sequelize.STRING(30),
        allowNull: false,
        validate: { 
          notEmpty: true 
        },
      },
      price: {
        type: Sequelize.DECIMAL(10,2),
        allowNull: false,
        validate: { 
          notEmpty: true 
        },
      },
      createdAt: {
        type: Sequelize.DATE(),
        allowNull: true
      },
      updatedAt: {
        type: Sequelize.DATE(),
        allowNull: true,
      }
    }),

  down: queryInterface => queryInterface.dropTable('BuildOrder')
};
