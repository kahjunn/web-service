module.exports = {
  up: (queryInterface, Sequelize) => 
    queryInterface.createTable('CPU', {
      id: {
        type: Sequelize.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true
      },
      name: {
        type: Sequelize.STRING(80),
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      brand: {
        type: Sequelize.STRING(100),
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      model: {
        type: Sequelize.STRING(80),
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      cache: {
        type: Sequelize.INTEGER,
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      coreCount: {
        type: Sequelize.INTEGER,
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      clockSpeed: {
        type: Sequelize.DECIMAL(5, 1),
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      boostClock: { 
        type: Sequelize.DECIMAL(5, 1),
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      supportedSocket: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      threads: {
        type: Sequelize.INTEGER,
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      coreFamily : {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      maxMemory: {
        type: Sequelize.INTEGER,
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      lithography: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      price: {
        type: Sequelize.DECIMAL(10, 2),
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      detail1: {
        type: Sequelize.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      detail2: {
        type: Sequelize.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      detail3: {
        type: Sequelize.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      type: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      createdAt: {
        type: Sequelize.DATE(),
        allowNull: true
      },
      updatedAt: {
        type: Sequelize.DATE(),
        allowNull: true,
      }
    }),

  down: queryInterface => queryInterface.dropTable('CPU')
};
