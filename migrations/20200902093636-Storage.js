module.exports = {
  up: (queryInterface, Sequelize) => 
    queryInterface.createTable('Storage', {
      id: {
        type: Sequelize.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true
      },
      name: {
        type: Sequelize.STRING(100),
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      brand: {
        type: Sequelize.STRING(100),
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      model: {
        type: Sequelize.STRING(80),
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      storageType: {
        type: Sequelize.STRING(3),
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      rpmSpeed: {
        type: Sequelize.STRING(80),
        allowNull: true,
      },
      volume: {
        type: Sequelize.INTEGER(10),
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      formFactor: { 
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      storageInterface: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      nvme: {
        type: Sequelize.STRING(3),
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      price: {
        type: Sequelize.DECIMAL(10, 2),
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      detail1: {
        type: Sequelize.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      detail2: {
        type: Sequelize.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      detail3: {
        type: Sequelize.TEXT,
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      type: {
        type: Sequelize.STRING,
        allowNull: false,
        validate: {
          notEmpty: true
        }
      },
      createdAt: {
        type: Sequelize.DATE(),
        allowNull: true
      },
      updatedAt: {
        type: Sequelize.DATE(),
        allowNull: true,
      }
    }),

  down: queryInterface => queryInterface.dropTable('Storage')
};
